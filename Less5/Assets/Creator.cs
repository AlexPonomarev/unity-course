﻿using UnityEngine;
using System.Collections;

public class Creator : MonoBehaviour 
{
	Ray ray;
	[SerializeField] private Collider Plane;
	public GameObject SphereRain;
	public GameObject Cube;
	private RaycastHit linehit;

	void Start () {
		StartCoroutine (Inst());

	}
	IEnumerator Inst ()
	{
	Vector3 v1=new Vector3 (Random.Range(-5,5), 7, Random.Range(-5,5)); 				// запись систем координат для дождя
	Instantiate (SphereRain, v1, Quaternion.identity);
	yield return new WaitForSeconds(1f);
	Returning ();
	}

	void OnMouseUp(){		

	}
		
	void Returning(){
		StartCoroutine (Inst ());
	}

	void Update ()
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out linehit))
			{
				if (linehit.collider.CompareTag ("Ground")) 
				{
					Vector3 MousePos = linehit.point;
					Debug.Log("Position");
					Instantiate (Cube, MousePos, Quaternion.identity);
				}		// передача данных позиции объекту
			}	
		}
	}				
}
