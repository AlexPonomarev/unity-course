﻿using System;
using UnityEngine;
using System.Collections;

//-------------------------------------------------//
public class colorchange : MonoBehaviour 
{
	[SerializeField] private Collider Sph;
	Ray ray;
	private RaycastHit linehit;

	void Start()
	{

	}

	void OnMouseDown()			//Обработка нажатия мыши на сферу
	{

		if (Input.GetMouseButtonDown (0)) {		// левая кнопка
			//if (Sph.gameObject.name = "Sphere")
			Destroy (this.gameObject);
			Debug.Log ("Pressed left click.");
		}
	}

	void Update ()
	{
		if (Input.GetMouseButtonDown (1)){		// правая кнопка
			this.gameObject.GetComponent<Renderer> ().material.color = Color.cyan;
			Debug.Log ("Pressed right click.");
		} 

		if (transform.position.y < 0) {
			Destroy (gameObject);		// удаление неокрашенных сфер, которые не взаимодействуют с Plane
		}
	}
}

/*							Справка по кнопкам мыши
if (Input.GetMouseButtonDown(0))		// левая кнопка
	Debug.Log("Pressed left click.");

if (Input.GetMouseButtonDown(1))		// правая кнопка
	Debug.Log("Pressed right click.");

if (Input.GetMouseButtonDown(2))
	Debug.Log("Pressed middle click.");
---------------------------------------

*/