﻿using UnityEngine;
using System.Collections;

public class gamestart : MonoBehaviour {



	[SerializeField] GameObject Cube1, Cube2, C_Ext, Sphere;
	[SerializeField] float x, y, z;
	private GameObject CubeG;

	void Start ()
	{
		Inicilization();
		sphere ();
		epoint ();
	}


	void Update () {

	}

	[SerializeField]int dX, dY, dZ;
	void Inicilization()
	{
		for (int y = 0; y < dY; y++)
		{

			for (int x = 0; x < dX; x++)
			{

				for (int z = 0; z < dZ; z++)
				{
					CreateCube(x, y, z);

				}
			}
		}
	}

	void CreateCube(float x, float y ,float z)
	{
		Vector3 Vec = new Vector3 (x, y, z);
		CubeG = Instantiate (Cube1, Vec, Quaternion.identity) as GameObject;
	}

	public void epoint(){
		Vector3 v2 = new Vector3 (x, y, z);
		Instantiate (C_Ext, v2, Quaternion.identity);
	}

	public void sphere(){
		Vector3 v3 = new Vector3 (Random.Range (0, dX), 9, Random.Range(0, dY));
		Instantiate (Sphere, v3, Quaternion.identity);	
	}
}