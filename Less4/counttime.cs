﻿using UnityEngine;
using System;
using System.Collections;

public class counttime: MonoBehaviour
{
	public event EventHandler<TimeSpawnEventArgs> OnTimeSpawn;
	[SerializeField]private float cooldownTime;
	private float time1;
	private float time2;

	void Start () {
		time1 = Time.time;
	}


	void Update () {
		time2 = Time.time - time1;
		if(cooldownTime<= time2)
		{
			time1 = Time.time;

			if (OnTimeSpawn != null)
			{
				OnTimeSpawn (this, new TimeSpawnEventArgs (this));
			}
		}
	}
	public class TimeSpawnEventArgs : EventArgs
	{
		public counttime SpownBubble { get; private set; }
		public TimeSpawnEventArgs(counttime spownObject)
		{
			SpownBubble = spownObject;
		}
	}
}