﻿using UnityEngine;
using System.Collections;

public class create : MonoBehaviour
{
	[SerializeField] private GameObject Sphere;

	private void Start()
	{
		GetComponent<counttime>().OnTimeSpawn += BubbleTimeToSpown;
	}

	private void CreateBubble()										// создание капли
	{
		Vector3 initialPosition = new Vector3 (UnityEngine.Random.Range (-3, 3), 5, UnityEngine.Random.Range (-3, 3));	//позиция
		GameObject newBubble = Instantiate (Sphere, initialPosition, Quaternion.identity) as GameObject;				//сама капля
		newBubble.transform.parent = transform;
	}

	private void BubbleTimeToSpown (object sender, counttime.TimeSpawnEventArgs args)
	{
		CreateBubble ();											// создание капли
	}

	private void OnDestroy()
	{
		GetComponent<counttime>().OnTimeSpawn -= BubbleTimeToSpown;	//отпись от события
	}
}