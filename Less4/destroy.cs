﻿using UnityEngine;
using System.Collections;

public class destroy : MonoBehaviour {
	
	void OnCollisionExit(Collision other) {
		Destroy (other.gameObject);
	}

	void Update () {
		if (transform.position.y < 0) {
			Destroy (gameObject);		// удаление неокрашенных сфер, которые не взаимодействуют с Plane
		}
	}
}
