﻿using UnityEngine;
using System.Collections;

public class create : MonoBehaviour 
{
	public GameObject Target;
	private RaycastHit linehit;

	void Start () {
		instant ();
	}

	void instant(){
		Vector3 tps = new Vector3 (0, 0, 0);
		Target = Instantiate (Target, tps, Quaternion.LookRotation(Vector3.down)) as GameObject;
		Target.SetActive (true);
	}

	void Update ()
	{
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out linehit)) {
				if (linehit.collider.CompareTag ("Plane")) {
					Target.transform.position = linehit.point;
					gameObject.SetActive (true);

					// подпись на таймер;
				}		
			}	
		}
	}
}