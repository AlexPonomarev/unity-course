﻿using UnityEngine;
using System;
using System.Collections;

public class timer: MonoBehaviour
{
	[SerializeField]private float timelife;
	[SerializeField]private float timeStart;
	private float timeLeft;

	public void Target(){
		gameObject.SetActive (true);
	}

	void Start () {
		timeStart = Time.time;
	}

	// Update is called once per frame
	void Update () {
		if (gameObject.activeSelf == true)
			timeLeft = Time.time - timeStart;
			if (timelife >= timeLeft) {
				timeStart = Time.time;
				gameObject.SetActive (false);
		}
	}
}
