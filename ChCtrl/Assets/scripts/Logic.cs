﻿using UnityEngine;
using System.Collections;

public class Logic : MonoBehaviour
{
	[SerializeField]private float TimeToDelete;
	[SerializeField]private float Endtime, StartTime;

	public void AimOn()
	{
		gameObject.SetActive (true);
	}

	public void Start()
	{
		StartTime = Time.time;
	}

	void Update()
	{
		if(gameObject.active == true)
		{

			Endtime = Time.time - StartTime;
			if (Endtime >= TimeToDelete)
			{
				StartTime = Time.time;
				gameObject.SetActive(false);

			}

		}
	}
}