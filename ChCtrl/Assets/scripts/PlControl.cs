﻿using UnityEngine;
using System.Collections;

public class PlControl : MonoBehaviour
{
	public GameObject _target;
	public float speed;
	public float seeDistance;
	[SerializeField] private GameObject AimPrefab;
	private GameObject Target;

	private void Start()
	{
		CreateAim ();
	}
	void Update ()
	{
		RaycastHit hit;
		if (Input.GetMouseButtonDown (0)) 
		{
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) 
			{
				if (hit.collider.CompareTag("Ground"))
				{
					Target.GetComponent<Logic> ().AimOn ();
					Target.transform.position = hit.point;

				}
			}
		}
		if (Vector3.Distance (transform.position, Target.transform.position) > seeDistance)
		{
			Move ();
		}
	}
	public  void CreateAim()
	{
		Vector3 AimPosition = new Vector3 (0,0,0);
		Target = Instantiate (AimPrefab,AimPosition,Quaternion.LookRotation(Vector3.down)) as GameObject;
		Target.SetActive (false);
	}
	private void Move ()
	{

		transform.LookAt (Target.transform);
		transform.Translate (new Vector3 (0, 0, speed * Time.deltaTime));

	}
}