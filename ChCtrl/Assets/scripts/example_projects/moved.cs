﻿using UnityEngine;
using System.Collections;

public class moved : MonoBehaviour {
	
	private Ray ray;
	private RaycastHit hit;
	private Vector3 target = new Vector3 ();
	private Vector3 lastTarget = new Vector3 ();

	private float mag;
	public float stopStart = 1.5f, speed = 5f; 
	public float heightPlayer = 1f;
	public float rotationSpeed = 100f;
	private bool walk;

	private float angelToTarget;
	private Vector3 dir;

	private void MoveTo ()
	{
		if (target != lastTarget) {
			if ((transform.position - target).sqrMagnitude > heightPlayer + 0.1f) {
				if (!walk) {
					Debug.Log ("Иду");
					walk = true;
				}
				mag = (transform.position - target).magnitude;
				transform.position = Vector3.MoveTowards (transform.position, target, mag > stopStart ? speed * UnityEngine.Time.deltaTime : Mathf.Lerp (speed * 0.5f, speed, mag / stopStart) * UnityEngine.Time.deltaTime);
				ray = new Ray (transform.position, -Vector3.up);
				if (Physics.Raycast (ray, out hit, 100f)) {
					transform.position = new Vector3 (transform.position.x, hit.point.y + heightPlayer, transform.position.z);
				}
			} else {
				lastTarget = target;
				if (walk) {
					Debug.Log ("Жду");
					walk = false;
				}
			}
		}
	}

	private void Update ()
	{
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit, 100f)) {
				if (hit.collider.CompareTag ("Ground")) {
					target = hit.point;
					Debug.Log("Координата получена");
				}		
			}	
		}
		MoveTo ();
	}

	private void LookThis(){
		if (target != lastTarget)
			CalculateAngel (target);
				if (angelToTarget > 3)
					transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation (dir), rotationSpeed * UnityEngine.Time.deltaTime);
	}

	private void CalculateAngel(Vector3 temp){
		dir = new Vector3 (temp.x, transform.position.y, temp.z) - transform.position;
		angelToTarget = Vector3.Angle (dir,transform.position);
	}
}