﻿using UnityEngine;
using System.Collections;

public class main : MonoBehaviour {
	[SerializeField] float speed;

	// public Transform target;
	public GameObject target;
	private float distance;
	public float step;
	private RaycastHit hit;

	public float enemyPlane;
	public float myPlane;

	// Use this for initialization
	void Start () {
	
	}


	public void CheckPlane()	// проверка плейна
	{

	}

	// Update is called once per frame
	void Update () 							// 			U P D A T E
	{
		MoveEnemy ();
		transform.LookAt(target.transform);
		distance = Vector3.Distance(transform.position, target.transform.position);

	}

	public void MoveEnemy()
	{
		if (distance >= 0.1) // если дистанция больше, то ...
		{
			step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards (transform.position, target.transform.position, step);

		}
		// Update ();
	}
}
