﻿using UnityEngine;
using System.Collections;

public class move : MonoBehaviour
{

	[SerializeField]private GameObject TargetObj;
	[SerializeField]private float moveSpeed;

	private GameObject Target;
	private Vector3 targetPoint;
	private RaycastHit hit;
	private bool IsMove;
	public float thrust;
	public Rigidbody Rb;


	private void Start()
	{
		Rb = GetComponent<Rigidbody>();
		CreateTarget();
	}
	private void Update()
	{
		MoveTarget(targetPoint);

		if (Input.GetMouseButtonDown(0))			// левая кнопка
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit))
			{
				if (hit.collider.CompareTag("gr_1"))		// проверка тега
				{
					Target.GetComponent<sublogic>().SetOn();	// логика
					Target.transform.position = hit.point;
					targetPoint = hit.point;
					IsMove = true;
				}
			}
		}
	}


	private void CreateTarget()					// создание цели
	{
		Vector3 TargetPosition = new Vector3(0, 0, 0);
		Target = Instantiate(TargetObj, TargetPosition, Quaternion.LookRotation(Vector3.down)) as GameObject;		//создание объекта
		Target.SetActive(false);				// отключение активности цели
	}
	private void MoveTarget(Vector3 target)
	{
		if (IsMove != false)
		{
			transform.LookAt(target);
			transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);	// перемещение к цели

		}
		if (Vector3.Distance(transform.position, target) < 1)
		{
			IsMove = false;
		}
	}
}