﻿using UnityEngine;
using System.Collections;

public class camera : MonoBehaviour {

	[SerializeField]public float angl = 50;
	[SerializeField]public float sens = 5f;

	float X,Y;
	float scroll;
	public float zoomStep = 0.25f; // чувствительность зума
	public float zoomMax = 10; // максимум
	public float zoomMin = 3; // мининимум
	public float currentZoom;
	public Vector3 offset;
	public Transform target;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton (2)) {			// средняя кнопка мыши
			transform.RotateAround (Vector3.zero, Vector3.up, angl * Time.deltaTime);		// Стабильно работающее вращение камеры по щелчку
		}
	}
}