﻿using UnityEngine;
using System.Collections;

public class sublogic : MonoBehaviour {
	[SerializeField] private float LifeT;
	[SerializeField] private float StartT, EndT;

	public void Start(){
		StartT = Time.deltaTime;
	}

	public void SetOn (){
		gameObject.SetActive(true);
	}

	public void Update(){
		if (gameObject.active == true) 
		{
			EndT = Time.time - StartT;
			if (EndT >= LifeT)
			{
				StartT = Time.time;
				gameObject.SetActive (false);
			}
		}
	}
}